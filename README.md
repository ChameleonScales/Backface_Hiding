# Backface-Hiding

This Blender add-on hides the backfacing or occluded mesh components (vertices, edges & faces) while in edit mode

It can be used to clear out your view when modeling or when doing retopology .

⚠️ At the moment, it only works as an operator which has to be executed each time you want to hide backfacing geometry, using either a button or a keyboard shortcut.

ℹ️ It is updated for Blender 4.0.2 and distributed under the GPL v3 license.

## Features :


* **Hide backfacing :** hide the backfacing geometry
* **Hide occluded :** hide geometry occluded by other or outside of the viewport's screen area.
* **Reveal & restore selection :** reveal hidden geometry and restore the selection you made before hiding.
* **Reveal & keep selection :** reveal hidden geometry and keep the current selection (all hidden gets deselected)

These functions can be accessed via existing buttons and shortcuts:

1. F5 up to F8, changeable in the add-on's preferences
2. Right side panel > Backface tab, also changeable in add-on's preferences

Feel free to ask for improvements or give feedback via [an issue report](https://gitlab.com/ChameleonScales/backface-hiding-2.8x/issues) or in the [blenderartists thread](https://blenderartists.org/t/addon-backface-hiding/700822)



### TODO:
- improve performace
- unify reveal operators in one that keeps the selection in visible geometry and restores the selection made in the hidden geometry
- auto mode that trgiggers the operator on each MMB release
- hide occluded as a toggle
- hide occluded should use non-selected objects as obstacles if possible
