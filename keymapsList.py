keymaps_items_dict = {"Hide backfacing Operator":['mesh.hide_backfacing', None, '3D View '
                                      'Generic', 'VIEW_3D', 'WINDOW',
                                      'F5', 'PRESS', False, False, False
                                      ],
                     "Hide Occluded Operator":['mesh.hide_occluded', None, '3D View '
                                      'Generic', 'VIEW_3D', 'WINDOW',
                                      'F6', 'PRESS', False, False, False
                                      ],
                     "Un-hide restore Operator":['mesh.unhide_restore', None, '3D View '
                                      'Generic', 'VIEW_3D', 'WINDOW',
                                      'F7', 'PRESS', False, False, False
                                      ],
                     "Un-hide keep Operator":['mesh.unhide_keep', None, '3D View '
                                      'Generic', 'VIEW_3D', 'WINDOW',
                                      'F8', 'PRESS', False, False, False
                                      ]
                     }