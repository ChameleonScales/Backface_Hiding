import bpy
import bmesh

class MemorizeSelection(bpy.types.PropertyGroup):

    # Store original selection :
    select_mode = {}
    dct = {}

class MESH_OT_restore_selection(bpy.types.Operator) :
    bl_idname = "mesh.restore_selection"
    bl_label = "Restore Selection"
    bl_description = "Restores the selection of vertices, edges and faces"
    bl_options = {'INTERNAL'}

    def execute(self, context) :
        obj = bpy.context.edit_object
        d = obj.data
        dct=obj.memorize_selection.dct
        objs= bpy.data.objects

        #restore select mode
        bpy.context.tool_settings.mesh_select_mode=obj.memorize_selection.select_mode["selectMode"]
        
        # delete dictionary keys for objects that have been deleted
        for key in list(dct.keys()):
            if objs.get(key[:-1]) is None:
                del dct[key]
        
        # select mesh components from dictionary. [:-1] removes letter added in store_selection
        for meshSet in list(dct.keys()):
                if objs[meshSet[:-1]].data.is_editmode:
                    o = objs[meshSet[:-1]]
                    bm = bmesh.from_edit_mesh(o.data)
            
                    if meshSet==meshSet[:-1]+'v':
                        bm.verts.ensure_lookup_table()
                        for v in dct[meshSet]:
                            bm.verts[v].select=True
                    elif meshSet==meshSet[:-1]+'e':
                        bm.edges.ensure_lookup_table()
                        for e in dct[meshSet]:
                            bm.edges[e].select=True
                    else:
                        bm.faces.ensure_lookup_table()
                        for f in dct[meshSet]:
                            bm.faces[f].select=True
        
        bmesh.update_edit_mesh(d)
        
        return {'FINISHED'}

class MESH_OT_store_selection(bpy.types.Operator):
    bl_idname = "mesh.store_selection"
    bl_label = "Store Selection"
    bl_description = "memorizes the mesh selection"
    bl_options = {'INTERNAL'}

    @classmethod
    def poll(cls, context):
        return context.object is not None and context.object.data.is_editmode

    def execute(self, context) :
        obj = context.object
        # store select mode
        obj.memorize_selection.select_mode["selectMode"] = bpy.context.tool_settings.mesh_select_mode[0:3]

        # store mesh component indices in dictionary
        dct=obj.memorize_selection.dct
        for o in bpy.context.objects_in_mode:
            bm = bmesh.from_edit_mesh(o.data)
            dct.update( {o.name+'v' : []} )
            dct.update( {o.name+'e' : []} )
            dct.update( {o.name+'f' : []} )
            for v in bm.verts:
                if v.select:
                    dct[o.name+'v'].append(v.index)
            for e in bm.edges:
                if e.select:
                    dct[o.name+'e'].append(e.index)
            for f in bm.faces:
                if f.select:
                    dct[o.name+'f'].append(f.index)
        return {'FINISHED'}

class HideBackfacingOperator(bpy.types.Operator):
    bl_idname = "mesh.hide_backfacing"
    bl_label = "Hide Backfacing"
    bl_description = "Hide backfacing geometry"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context) :
        bpy.ops.mesh.store_selection()
        
        # go in face selection mode and un-hide and deselect everything
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='FACE')
        bpy.ops.mesh.reveal()
        bpy.ops.mesh.select_all(action='DESELECT')
        
        # Add a plane and rotate it to face the view :
        rotView=bpy.context.region_data.view_matrix.transposed().to_euler()
        
        bpy.ops.mesh.primitive_plane_add(size=1,location=(0,0,0),rotation=rotView)
        
        # Make the plane active (for later deletion) :
        bpy.ops.object.mode_set(mode='OBJECT')
        for face in bpy.context.object.data.polygons:
            if face.select:
                bpy.context.object.data.polygons.active = face.index
        
        # Select and hide polygons turning away from created plane
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_similar(type='FACE_NORMAL', threshold=0.5)
        bpy.ops.mesh.hide(unselected=True)
        bpy.ops.mesh.select_all(action='DESELECT')
        
        # Select the plane and delete it
        bpy.ops.object.mode_set(mode='OBJECT')
        for face in bpy.context.object.data.polygons:
            if bpy.context.object.data.polygons.active==face.index:
                face.select=True
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.delete(type='FACE')
        
        bpy.ops.mesh.restore_selection()
        
        return {'FINISHED'}

class HideOccludedOperator(bpy.types.Operator):
    bl_idname = "mesh.hide_occluded"
    bl_label = "Hide occluded"
    bl_description = "Hides view-occluded geometry"
    bl_options = {'REGISTER', 'UNDO'}
    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        
        bpy.ops.mesh.store_selection()
        
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='FACE')
        bpy.ops.mesh.reveal()
        
        # Store original "Limit selection to visible" :
        original_occlude_mode = bpy.context.space_data.shading.show_xray
        
        # Enable "Limit selection to visible" :
        bpy.context.space_data.shading.show_xray = False
        
        # Border select geometry with a rectangle that fills the 3DView Area
        
        bpy.ops.view3d.select_box(xmin=0, xmax=bpy.context.area.width, ymin=0, ymax=bpy.context.area.height, wait_for_input=False, mode='SET')
        
        # Hide non-selected geometry
        bpy.ops.mesh.hide(unselected=True)
        
        bpy.ops.mesh.select_all(action='DESELECT')
        
        # Retore original "Limit selection to visible" :
        bpy.context.space_data.shading.show_xray = original_occlude_mode

        bpy.ops.mesh.restore_selection()
        
        return {'FINISHED'}

def ShowMessageBox(title = "Message Box", icon = 'INFO'):
    def draw(self, context):
        self.layout.label()
    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)

class UnhideRestoreOperator(bpy.types.Operator):
    bl_idname = "mesh.unhide_restore"
    bl_label = "Reveal & restore selection"
    bl_description = "reveals the hidden faces and restores the mesh selection you made before hiding"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        if context.object.memorize_selection.dct:
            bpy.ops.mesh.reveal()
            bpy.ops.mesh.select_all(action='DESELECT')
            bpy.ops.mesh.restore_selection()
        else:
            ShowMessageBox("Use a Hide button first")
        return {'FINISHED'}

class UnhideKeepOperator(bpy.types.Operator):
    bl_idname = "mesh.unhide_keep"
    bl_label = "Reveal & keep selection"
    bl_description = "reveals the hidden faces and keeps the current mesh selection"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):

        bpy.ops.mesh.select_all(action='INVERT')
        bpy.ops.mesh.reveal()
        bpy.ops.mesh.select_all(action='INVERT')
        
        return {'FINISHED'}

CLASSES = [MemorizeSelection,
           MESH_OT_restore_selection,
           MESH_OT_store_selection,
           HideBackfacingOperator,
           HideOccludedOperator,
           UnhideRestoreOperator,
           UnhideKeepOperator
           ]

def register():
    for cls in CLASSES:
       bpy.utils.register_class(cls)
       bpy.types.Object.memorize_selection = bpy.props.PointerProperty(type=MemorizeSelection)

def unregister():

    for cls in CLASSES:
       bpy.utils.unregister_class(cls)
