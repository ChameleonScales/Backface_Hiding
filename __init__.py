# ##### BEGIN GPL LICENSE BLOCK #####
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published by
#    the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
	"name": "Backface Hiding",
	"description": "Hide backfacing or occluded geometry in Edit mode",
	"author": "Caetano Veyssières (ChameleonScales) with precious help from Secrop and Pistiwique",
	"version": (0, 4, 6),
	"blender": (4, 0, 2),
	"location": "View3D > Sidebar > Backface Tab > Backface Hiding",
	"doc_url": "https://gitlab.com/ChameleonScales/backface-hiding-2.8x/blob/master/README.md",
	"tracker_url": "https://gitlab.com/ChameleonScales/backface-hiding-2.8x/issues",
	"category": "3D View" }

import bpy
import rna_keymap_ui

from bpy.types import AddonPreferences
from bpy.props import (EnumProperty,
						StringProperty,
						BoolProperty)
from . import operatorsList
from . import keymapsList
from .keymapsList import keymaps_items_dict

# -----------------------------------------------------------------------------
# Preferences
# -----------------------------------------------------------------------------
def update_backfacehiding_tab_name(self, context):
	is_panel = hasattr(bpy.types, 'BACKFACEHIDING_PT_ui')

	if is_panel:
		try:
			bpy.utils.unregister_class(BACKFACEHIDING_PT_ui)
		except:
			pass
	BACKFACEHIDING_PT_ui.bl_category = self.tab_name
	bpy.utils.register_class(BACKFACEHIDING_PT_ui)

def prefs():
	return bpy.context.preferences.addons[__name__].preferences

class BackfaceHidingPreferences(AddonPreferences):
	bl_idname = __name__

	prefs_tabs: EnumProperty(
		items=(('info', "Info", "ADDON INFO"),
			('options', "Options", "ADDON OPTIONS"),
			('keymaps', "Keymaps", "CHANGE KEYMAPS")),
		default='keymaps')

	tab_name : StringProperty(description="Choose a name for the tab in the side panel", default="Backface", update=update_backfacehiding_tab_name)

	hide_backfacing_btn : BoolProperty(description="Hide Backfacing Button", default=True)
	hide_occluded_btn : BoolProperty(description="Hide Occluded Button", default=True)
	reveal_restore_btn : BoolProperty(description="Reveal & restore selection Button", default=True)
	reveal_keep_btn : BoolProperty(description="Reveal & keep selection Button", default=True)

	def draw(self, context):
		wm = context.window_manager
		layout = self.layout

		row= layout.row(align=True)
		row.prop(self, "prefs_tabs", expand=True)
		if self.prefs_tabs == 'info':
			layout.label(text="This add-on is for use in Edit mode.")
			layout.label(text="it's different to backface culling in that it")
			layout.label(text="hides all the overlays as well (vertices, edges, faces)")
			layout.label(text="However, it doesn't update in real time, only each time")
			layout.label(text="you press the button (or shortcut)")

		if self.prefs_tabs == 'options':
			box = layout.box()

			row = box.row(align=True)
			row.label(text="Tab Name:")
			row.prop(self, "tab_name", text="")
			box.label(text="Show/Hide Buttons:")
			box.prop(self, "hide_backfacing_btn", text="Hide Backfacing")
			box.prop(self, "hide_occluded_btn", text="Hide Occluded")
			box.prop(self, "reveal_restore_btn", text="Reveal & Restore Selection")
			box.prop(self, "reveal_keep_btn", text="Reveal & Keep Selection")

		# KEYMAPS
		if self.prefs_tabs == 'keymaps':
			wm = bpy.context.window_manager
			draw_keymap_items(wm, layout)

addon_keymaps = []

def draw_keymap_items(wm, layout):
	kc = wm.keyconfigs.user

	for name, items in keymaps_items_dict.items():
		kmi_name, kmi_value, km_name = items[:3]
		box = layout.box()
		split = box.split()
		col = split.column()
		col.label(text=name)
		col.separator()
		km = kc.keymaps[km_name]
		get_hotkey_entry_item(kc, km, kmi_name, kmi_value, col)

def get_hotkey_entry_item(kc, km, kmi_name, kmi_value, col):
	if km.keymap_items.get(kmi_name):
		col.context_pointer_set('keymap', km)
		rna_keymap_ui.draw_kmi(
			[], kc, km, km.keymap_items[kmi_name], col, 0)
	else:
		col.label(text=f"No hotkey entry found for {kmi_name}")
		col.operator(TEMPLATE_OT_Add_Hotkey.bl_idname, icon='ADD')

class TEMPLATE_OT_Add_Hotkey(bpy.types.Operator):
	''' Add hotkey entry '''
	bl_idname = "template.add_hotkey"
	bl_label = "Add Hotkeys"
	bl_options = {'REGISTER', 'INTERNAL'}

	def execute(self, context):
		add_hotkey()

		self.report({'INFO'},
					"Hotkey added in User Preferences -> Input -> Screen -> Screen (Global)")
		return {'FINISHED'}

def add_hotkey():
	wm = bpy.context.window_manager
	kc = wm.keyconfigs.addon
	# In background mode, there's no such thing has keyconfigs.user,
	# because headless mode doesn't need key combos.
	# So, to avoid error message in background mode, we need to check if
	# keyconfigs is loaded.
	if not kc:
		return

	for items in keymaps_items_dict.values():
		kmi_name, kmi_value, km_name, space_type, region_type = items[:5]
		eventType, eventValue, ctrl, shift, alt = items[5:]
		km = kc.keymaps.new(name = km_name, space_type = space_type,
							region_type=region_type)
		kmi = km.keymap_items.new(kmi_name, eventType,
									eventValue, ctrl = ctrl, shift = shift,
									alt = alt
									)
		if kmi_value:
			kmi.properties.name = kmi_value

		kmi.active = True

	addon_keymaps.append((km, kmi))

def remove_hotkey():
	''' clears all addon level keymap hotkeys stored in addon_keymaps '''

	kmi_values = [item[1] for item in keymaps_items_dict.values() if item]
	kmi_names = [item[0] for item in keymaps_items_dict.values() if item not in ['wm.call_menu', 'wm.call_menu_pie']]

	for km, kmi in addon_keymaps:
		# remove addon keymap for menu and pie menu
		if hasattr(kmi.properties, 'name'):
			if kmi_values:
				if kmi.properties.name in kmi_values:
					km.keymap_items.remove(kmi)

		# remove addon_keymap for operators
		else:
			if kmi_names:
				if kmi.name in kmi_names:
					km.keymap_items.remove(kmi)

	addon_keymaps.clear()

class BACKFACEHIDING_PT_ui(bpy.types.Panel):
	bl_label = "Backface Hiding"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "Edit"
	bl_context = "mesh_edit"

	def draw(self, context):
		layout = self.layout
		scene = context.scene

		if prefs().hide_backfacing_btn:
			layout.operator("mesh.hide_backfacing", text="Hide Backfacing", icon='ORIENTATION_NORMAL')
		if prefs().hide_occluded_btn:
			layout.operator("mesh.hide_occluded", text="Hide Occluded", icon='XRAY')
		layout.separator(factor=3.0)
		if prefs().reveal_restore_btn:
			layout.operator("mesh.unhide_restore", text="Reveal & Restore Selection", icon='HIDE_OFF')
		if prefs().reveal_keep_btn:
			layout.operator("mesh.unhide_keep", text="Reveal & Keep Selection", icon='HIDE_OFF')

CLASSES = [TEMPLATE_OT_Add_Hotkey,
			BackfaceHidingPreferences,
			BACKFACEHIDING_PT_ui
			]

def register():
	operatorsList.register()
	for cls in CLASSES:
		bpy.utils.register_class(cls)
	context = bpy.context
	prefs = context.preferences.addons[__name__].preferences
	update_backfacehiding_tab_name(prefs, context)
	# hotkey setup
	add_hotkey()

def unregister():
	# hotkey cleanup
	remove_hotkey()

	for cls in CLASSES:
		bpy.utils.unregister_class(cls)

	operatorsList.unregister()
